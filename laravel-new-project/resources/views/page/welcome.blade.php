@extends('layouts.master')

@section('title')
    <h1>Halaman Welcome</h1>
@endsection

@section('subtitle')
    <h2>Halaman Welcome</h2>
@endsection

@section('content')
<h1>Selamat Datang {{ $namaDepan }} {{ $namaBelakang }}</h1>
<h3>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h3>
<h5><a href="/">Log Out</a></h5>
@endsection
