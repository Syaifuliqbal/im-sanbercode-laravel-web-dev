@extends('layouts.master')

@section('title')
<h1>Buat Account Baru!</h1>
    
@endsection

@section('subtitle')
<h3>Sign Up Form</h3>
    
@endsection

@section('content')
<form action="/welcome" method="post">
    @csrf
<label>First name:</label>
<br><br>
<input type="text" name="first_name">
<br><br>
<label>Last name:</label>
<br><br>
<input type="text" name="last_name">
<br><br>
<label>Gender:</label>
<br><br>
<input type="radio" name="gender">Male<br>
<input type="radio" name="gender">Female<br>
<input type="radio" name="gender">Other<br>
<br>
<label>Nationality:</label>
<br><br>
<select name="nationality">
    <option value="nationality">Indonesian</option>
    <option value="nationality">Singaporean</option>
    <option value="nationality">Malaysian</option>
    <option value="nationality">Australian</option>
</select>
<br><br>
<label>Language Spoken:</label>
<br><br>
<input type="checkbox" name="language_spoken">Bahasa Indonesia <br>
<input type="checkbox" name="language_spoken">English <br>
<input type="checkbox" name="language_spoken">Other <br>
<br>
<label>Bio:</label>
<br><br>
<textarea cols="30" rows="10"></textarea>
<br><br>
<input type="submit" value="kirim">
</form>
    
@endsection
