@extends('layouts.master')

@section('title')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
@if (Session::get('success'))
<div class="alert alert-success">{{ Session::get('success') }}</div>
@endif
@auth
<a href="/genre/create" class="btn btn-primary btn-sm my-2">Tambah Data</a>
@endauth
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($genre as $key=>$value)
          <tr>
              <td>{{$key + 1}}</th>
              <td>{{$value->nama}}</td>
              <td>
                <a href="/genre/{{ $value->id }}" class="btn btn-success btn-sm my-2">Detail</a>
                @auth
                <form onsubmit="return confirm('Yakin akan menghapus data ini?')" class="d-inline" action="/genre/{{ $value->id }}" method="POST">
                  @method('delete')
                  @csrf
                  <a href="/genre/{{ $value->id }}/edit" class="btn btn-warning btn-sm my-2">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
                @endauth
              </td>
          </tr>
      @empty
          <tr colspan="3">
              <td>No data</td>
          </tr>  
      @endforelse              
  </tbody>
</table>
    
@endsection