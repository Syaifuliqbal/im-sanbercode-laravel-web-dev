@extends('layouts.master')

@section('title')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
<form action="/genre/{{ $genre->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="nama">Nama Lengkap</label>
      <input type="text" class="form-control" id="nama" name="nama" value="{{ $genre->nama }}">
      @error('nama')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
  @enderror
    </div>
    <a href="/genre" class="btn btn-secondary">Batal</a>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>

    
@endsection