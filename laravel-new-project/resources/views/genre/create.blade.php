@extends('layouts.master')

@section('title')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">Nama Lengkap</label>
      <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}">
      @error('nama')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
  @enderror
    </div>
    <a href="/genre" class="btn btn-secondary">Batal</a>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
@endsection