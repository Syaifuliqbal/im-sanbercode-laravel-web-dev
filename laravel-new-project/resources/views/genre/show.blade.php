@extends('layouts.master')

@section('title')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
    <h1 class="text-primary">Ini Adalah Salah Satu Genre {{ $genre->nama }} Dalam Sebuah Film</h1>
    <a href="/genre" class="btn btn-secondary">Kembali</a>
@endsection