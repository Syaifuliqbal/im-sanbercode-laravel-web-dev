@extends('layouts.master')

@section('title')
<h1>{{ $title }}</h1>
@endsection

@section('content')

<img src="{{ asset('image/' . $film->poster) }}" class="card-img-top" alt="...">
<h3 class="text-primary">{{ $film->judul }}</h3>
<h5 class="text">{{ $film->tahun }}</h5>
<p>{{ $film->ringkasan }}</p>

<a href="/film" class="btn btn-secondary btn-sm my-2">Kembali</a>

@endsection