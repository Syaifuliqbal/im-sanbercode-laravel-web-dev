@extends('layouts.master')

@section('title')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="judul">Judul Film</label>
      <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul') }}">
      @error('judul')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
  @enderror
    </div>
    <div class="form-group">
        <label for="ringkasan">Ringkasan Film</label>
        <textarea class="form-control" name="ringkasan" id="ringkasan">{{ old('ringkasan') }}</textarea> 
        @error('ringkasan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
      </div>
      <div class="form-group">
        <label for="tahun">Tahun</label>
        <input type="text" class="form-control" id="tahun" name="tahun" value="{{ old('tahun') }}">
        @error('tahun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
      </div>
      <div class="form-group">
        <label>Poster Film</label>
        <input type="file" class="form-control" id="poster" name="poster" value="{{ old('poster') }}">
        @error('poster')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
      </div>
      <div class="form-group">
        <label>Genre Film</label>
       <select name="genre_id" id="genre_id" class="form-control">
       <option value="">Pilih Genre Film</option>
       @forelse ($genre as $item)
           <option value="{{ $item->id }}">{{ $item->nama }}</option>
       @empty
       <option value="">Tidak Ada Genre Film</option>
       @endforelse
    </select>
        @error('genre_id')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
      </div>
    <a href="/film" class="btn btn-secondary">Batal</a>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
@endsection