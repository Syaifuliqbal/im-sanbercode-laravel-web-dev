@extends('layouts.master')

@section('title')
<h1>{{ $title }}</h1>
@endsection

@section('content')
@if (Session::get('success'))
<div class="alert alert-success">{{ Session::get('success') }}</div>
@endif

@auth
    
<a href="/film/create" class="btn btn-primary btn-sm my-2">Tambah Data</a>
@endauth

<div class="row">
    @forelse ($film as $item)
    <div class="col-4">
    <div class="card">
        <img src="{{ asset('image/' . $item->poster) }}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">{{ $item->judul }}</h5>
          <p class="card-text">{{ Str::limit($item->ringkasan, 100) }}</p>
          <a href="/film/{{ $item->id }}" class="btn btn-info btn-sm btn-block">Detail</a>
        @auth
        <div class="row mt-3">
          <div class="col">
              <a href="/film/{{ $item->id }}/edit" class="btn btn-warning btn-sm btn-block">Edit</a>
          </div>

          <div class="col">
              <form onsubmit="return confirm('Yakin akan menghapus data ini?')" class="d-inline" action="film/{{ $item->id }}" method="post">
              @csrf
          @method('delete')
          <input type="submit" value="delete" class="btn btn-danger btn-sm btn-block"></form>
          </div>

        </div>
        @endauth
         
        </div>
      </div>
    </div>
    @empty
        <div class="alert danger">Tidak Ada Film</div>
    @endforelse
</div>
@endsection