<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset ('adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        @auth
        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        @endauth

        @guest
        <a href="#" class="d-block">Anda Belum Login</a>
        @endguest
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @auth
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Table
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/table" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Table</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/data-tables" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Table</p>
              </a>
            </li>
          </ul>
        </li>
        @endauth
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-ellipsis-h"></i>
            <p>
              Film
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/film" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Daftar Film</p>
              </a>
            </li>
          </ul>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/cast" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Daftar Cast</p>
              </a>
            </li>
          </ul>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/genre" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Daftar Genre</p>
              </a>
            </li>
          </ul>
        </li>
        @guest
        <li class="nav-item bg-info">
          <a href="/login" class="nav-link">
            <p>Login</p>
          </a>
        </li>
        @endguest
        
        @auth
        <li class="nav-item">
          <a href="/profile" class="nav-link">
            <i class="nav-icon fas fa-user"></i>
            <p>
              My Profile
            </p>
          </a>
        </li>
        <li class="nav-item bg-danger">
          <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
             Logout
            </a>
            
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
            </form>
          </li>
          @endauth

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
