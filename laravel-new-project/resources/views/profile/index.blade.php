@extends('layouts.master')

@section('title')
    <h1>{{ $title }}</h1>
@endsection
@section('content')
@if (Session::get('success'))
<div class="alert alert-success">{{ Session::get('success') }}</div>
@endif
<form action="/profile/{{ $detailProfile->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control" id="umur" name="umur" value="{{ $detailProfile->umur }}">
      @error('umur')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
  @enderror
    </div>
    <div class="form-group">
        <label>Biodata</label>
        <textarea class="form-control" name="bio" id="bio">{{  $detailProfile->bio  }}</textarea> 
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
      </div>
      <div class="form-group">
        <label>Alamat</label>
        <textarea class="form-control" name="alamat" id="alamat">{{  $detailProfile->alamat  }}</textarea> 
        @error('alamat')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
      </div>
    <a href="/profile" class="btn btn-secondary">Batal</a>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
@endsection