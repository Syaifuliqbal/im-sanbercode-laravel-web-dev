@extends('layouts.master')

@section('title')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
    <h1 class="text-primary">{{ $cast->nama }}</h1>
    <p>{{ $cast->umur }}</p>
    <p>{{ $cast->bio }}</p>
    <a href="/cast" class="btn btn-secondary">Kembali</a>
@endsection