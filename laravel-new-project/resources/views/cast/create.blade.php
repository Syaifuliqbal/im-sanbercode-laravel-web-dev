@extends('layouts.master')

@section('title')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">Nama Lengkap</label>
      <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}">
      @error('nama')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
  @enderror
    </div>
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur') }}">
      @error('umur')
      <div class="alert alert-danger" role="alert">
          {{ $message }}
      </div>
  @enderror
    </div>
    <div class="form-group">
      <label for="nama">Bio</label>
      <textarea class="form-control" name="bio" id="bio">{{ old('bio') }}</textarea> 
      @error('bio')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
  @enderror
    </div>
    <a href="/cast" class="btn btn-secondary">Batal</a>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>

    
@endsection