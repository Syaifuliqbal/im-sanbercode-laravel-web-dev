<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmModel extends Model
{
    use HasFactory;
    protected $table = "film";
    protected $fillable = ["judul", "ringkasan", "tahun", "poster", "genre_id"];
    public $timestamps = false;
}
