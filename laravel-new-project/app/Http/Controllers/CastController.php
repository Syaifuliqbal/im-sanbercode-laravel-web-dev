<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CastController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'detail']);
    }
    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.cast', [
            'cast' => $cast,
            'title' => 'Halaman Data Cast',
        ]);
    }
    public function create()
    {
        return view('cast.create', [
            'title' => 'Halaman Tambah Data',
        ]);
    }
    public function store(Request $request)
    {
        Session::flash('nama', $request->nama);
        Session::flash('umur', $request->umur);
        Session::flash('bio', $request->bio);
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|numeric',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'Nama Lengkap Wajib diisi',
                'umur.required' => 'Umur Wajib diisi',
                'umur.numeric' => 'Form Umur hanya bisa diisi angka',
                'bio.required' => 'Bio Wajib diisi',

            ]
        );
        DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);
        return redirect('cast')->with('success', 'Berhasil Tambah Data');
    }
    public function show($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.detail', [
            'cast' => $cast,
            'title' => 'Halaman Detail Data Cast',
        ]);
    }
    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.edit', [
            'cast' => $cast,
            'title' => 'Halaman Edit Data Cast',
        ]);
    }
    public function update($id, Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|numeric',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'Nama Lengkap Wajib diisi',
                'umur.required' => 'Umur Wajib diisi',
                'umur.numeric' => 'Form Umur hanya bisa diisi angka',
                'bio.required' => 'Bio Wajib diisi',

            ]
        );
        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );
        return redirect('cast')->with('success', 'Berhasil Ubah Data');
    }
    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('cast')->with('success', 'Berhasil Delete Data');
    }
}
