<?php

namespace App\Http\Controllers;

use App\Models\GenreModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function index()
    {
        $genre = GenreModel::all();
        return view('genre.genre', compact('genre'), [
            'title' => 'Halaman Data Genre',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = GenreModel::all();
        return view('genre.create', compact('genre'), [
            'title' => 'Halaman Data Genre',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Session::flash('nama', $request->nama);
        $this->validate(
            $request,
            [
                'nama' => 'required',
            ],
            [
                'nama.required' => 'Nama Lengkap Wajib diisi',
            ]
        );
        GenreModel::create([
            'nama' => $request->nama,
        ]);

        return redirect('genre')->with('success', 'Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = GenreModel::find($id);
        return view('genre.show', compact('genre'), [
            'title' => 'Halaman Detail Genre',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = GenreModel::find($id);
        return view('genre.edit', compact('genre'), [
            'title' => 'Halaman Edit Genre',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required',
            ],
            [
                'nama.required' => 'Nama Lengkap Wajib diisi',
            ]
        );
        $genre = GenreModel::find($id);
        $genre->nama = $request->nama;
        $genre->update();
        return redirect('genre')->with('success', 'Berhasil Ubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genre = GenreModel::find($id);
        $genre->delete();
        return redirect('genre')->with('success', 'Berhasil Hapus Data');
    }
}
