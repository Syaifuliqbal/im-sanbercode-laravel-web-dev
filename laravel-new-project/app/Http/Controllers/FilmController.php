<?php

namespace App\Http\Controllers;

use App\Models\FilmModel;
use App\Models\GenreModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function index()
    {
        $film = FilmModel::all();
        return view('film.film', [
            'film' => $film,
            'title' => 'Halaman Data Film',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = GenreModel::all();
        return view('film.create', [
            'genre' => $genre,
            'title' => 'Halaman Data Film',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'judul' => 'required',
                'ringkasan' => 'required',
                'tahun' => 'required|numeric',
                'poster' => 'required|mimes:jpg,jpeg,png',
                'genre_id' => 'required',
            ],
            [
                'judul.required' => 'Judul Film Wajib diisi',
                'ringkasan.required' => 'Ringkasan Film Wajib diisi',
                'tahun.required' => 'Tahun Film Wajib diisi',
                'tahun.numeric' => 'Form Tahun Wajib diisi dengan angka',
                'poster.required' => 'Poster Belum di pilih',
                'genre_id.required' => 'Genre Film Wajib Pilih',
            ]
        );

        $imageName = time() . '.' . $request->poster->extension();
        $request->poster->move(public_path('image'), $imageName);

        FilmModel::create([
            'judul' => $request['judul'],
            'ringkasan' => $request['ringkasan'],
            'tahun' => $request['tahun'],
            'poster' => $imageName,
            'genre_id' => $request['genre_id'],
        ]);

        // $film = new FilmModel;
        // $film->judul = $request->judul;
        // $film->ringkasan = $request->ringkasan;
        // $film->tahun = $request->tahun;
        // $film->poster = $request->poster;
        // $film->genre_id = $request->genre_id;

        // $film->save();

        return redirect('film')->with('success', 'Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = FilmModel::find($id);
        return view('film.show', [
            'film' => $film,
            'title' => 'Halaman Detail Film',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = FilmModel::find($id);
        $genre = GenreModel::all();
        return view('film.edit', [
            'film' => $film,
            'genre' => $genre,
            'title' => 'Halaman Edit Film',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'judul' => 'required',
                'ringkasan' => 'required',
                'tahun' => 'required|numeric',
                'poster' => 'mimes:jpg,jpeg,png',
                'genre_id' => 'required',
            ],
            [
                'judul.required' => 'Judul Film Wajib diisi',
                'ringkasan.required' => 'Ringkasan Film Wajib diisi',
                'tahun.required' => 'Tahun Film Wajib diisi',
                'tahun.numeric' => 'Form Tahun Wajib diisi dengan angka',
                'genre_id.required' => 'Genre Film Wajib Pilih',
            ]
        );
        $film = FilmModel::find($id);

        if ($request->has('poster')) {
            $path = 'image/';
            File::delete($path . $film->poster);

            $imageName = time() . '.' . $request->poster->extension();
            $request->poster->move(public_path('image'), $imageName);

            $film->poster = $imageName;
            $film->save();
        }

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->save();

        return redirect('film')->with('success', 'Berhasil Ubah Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = FilmModel::find($id);
        $path = 'image/';
        File::delete($path . $film->poster);

        $film->delete();
        return redirect('film')->with('success', 'Berhasil Hapus Data');
    }
}
